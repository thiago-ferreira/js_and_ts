# JavaScript

This repository groups all exercises for this task.

## Structure

Inside the `test` folder, you'll find two other folders: `lang` and `browser`.

Exercises 1 and 2 are inside `lang` and focus on plain JavaScript features. Exercise 3 is inside `browser` and address the JavaScript's behavior within a browser context.

## How to run

First step, install all the dependencies with Yarn package manager

```bash
yarn install
```

You can use NPM instead, it also works

```bash
npm install
```

To check if your layout is following ESLint rules, run

```bash
yarn lint
```

if there are any errors, it will show how to fix it. Don't forget to fix them all before submitting for evaluation.

Now you can run your tests:

- for `lang`-related tests, run `yarn test:lang`
- for `browser`-related tests, run `yarn test:browser`
- for all tests, run `yarn test`

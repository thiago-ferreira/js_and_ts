const test = require('ava');

function scaffoldStructure(document, data) {
  data.sort((a, b) => {
    // This will sort by both name and, with they are the same, by email address
    a_text = a.name.toLowerCase() + a.email.toLowerCase();
    b_text = b.name.toLowerCase() + b.email.toLowerCase();

    if (a_text < b_text) return -1;
    else if (a_text > b_text) return 1;
    else return 0;
  });

  const list_node = document.createElement('ul');
  data.forEach(entry => {
    let list_element_node = document.createElement('li');
    list_element_node.className = 'name';

    list_element_node.innerHTML =
      '<p><strong>' + entry.name + '</strong> - <u>' + entry.email + '</u></p>';

    list_node.append(list_element_node);
  });
  document.body.append(list_node);
}

// Creates structure of desired page from data before test. This way,
// tests only have to acess the DOM.
// IMPORTANT: all tests on this file should NOT modify de structure of the original DOM,
// since all of them are using the same document for testing
test.before('generating page from data', t => {
  const data = [
    { name: 'Bernardo', email: 'b.ern@mail.com' },
    { name: 'Carla', email: 'carl@mail.com' },
    { name: 'Fabíola', email: 'fabi@mail.com' }
  ];

  scaffoldStructure(document, data);
  t.context.data = data; // Acess to original data for all tests
  t.pass();
});

test('the creation of a page from scratch', t => {
  const nameNodes = document.querySelectorAll('.name');
  t.is(nameNodes.length, t.context.data.length);
});

test('presence of a unsorted list', t => {
  const element = document.querySelector('ul');
  t.not(element, null);
});

test("has items with class 'name'", t => {
  const element = document.querySelector('li.name');
  t.not(element, null);
});

test('text in alphabetic order', t => {
  const elements = document.querySelectorAll('li.name');
  const item_text = [];
  elements.forEach(element => {
    item_text.push(element.textContent);
  });

  for (i = 0; i < item_text.length - 1; i++) {
    // If text is unsorted
    if (item_text[i] > item_text[i + 1]) {
      t.fail();
    }
  }

  t.pass();
});

test("content has specific style '[bold text] - [underlined text]'", t => {
  const elements = document.querySelectorAll('li.name');
  elements.forEach(element => {
    let item_text = element.childNodes[0]; // <p> tag
    let tag_names = [
      item_text.childNodes[0].tagName.toLowerCase(),
      item_text.childNodes[2].tagName.toLowerCase()
    ];

    if (
      (tag_names[0] == 'b' || tag_names[0] == 'strong') &&
      tag_names[1] == 'u'
    )
      t.pass();
    else t.fail();
  });
});

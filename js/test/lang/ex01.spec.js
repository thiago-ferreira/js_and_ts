function objectFromArrays(keys, values) {
  if (keys.length != values.length)
    throw "Keys and values arrays sizes don't match";

  let object = {};
  for (i = 0; i < keys.length; i++) {
    object[keys[i]] = values[i];
  }

  return object;
}

test('Creating objects from arrays', () => {
  const keys = ['name', 'email', 'username'];
  const values = ['Bruno', 'bruno@mail.com', 'bruno'];

  const finalObject = {
    name: 'Bruno',
    email: 'bruno@mail.com',
    username: 'bruno'
  };

  expect(objectFromArrays(keys, values)).toEqual(finalObject);
});

function reverse(a, b) {
  if (a < b) return 1;
  else if (a > b) return -1;
  else return 0;
}

function indexer(element, index) {
  return '' + (index + 1) + '. ' + element;
}

function shorterThan6(word) {
  return word.length < 6;
}

describe('array functions', () => {
  // NOTE:
  // A necessary change to the test is the substitution of the method 'toBe()' to
  // 'toEqual()', since the correct result and the expected value from the test are arrays
  // and we need to check for deep equality, which is done by the latter
  test('reversed indexed arrays', () => {
    const names = ['Marina', 'Camila', 'Alberto', 'Felipe', 'Mariana'];

    const test = names.sort(reverse).map(indexer);
    const correct = [
      '1. Marina',
      '2. Mariana',
      '3. Felipe',
      '4. Camila',
      '5. Alberto'
    ];
    expect(test).toEqual(correct);
  });

  // NOTE:
  // This test, too, was modified: For the same reason as the test above, we need to use the
  // method 'toEqual()' in place of 'toBe()' here
  test('filtering lists', () => {
    const techComps = [
      'microsoft',
      'google',
      'apple',
      'ibm',
      'amazon',
      'facebook'
    ];

    const shortNames = techComps.filter(shorterThan6);
    const correctShortNames = ['apple', 'ibm'];

    expect(shortNames).toEqual(correctShortNames);
  });
});

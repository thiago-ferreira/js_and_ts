function createUser(name, email, password, passwordConfirmation) {
  if (name.length <= 0 || name.length > 64) {
    throw new Error('validation error: invalid name size');
  }

  // Change: part of regex 'com(\.[a-z]{2})' changed for 'com(\.[a-z]{2})*'
  // Change made for including e-mails ending with '.com' only
  if (!email.match(/[a-z]+@[a-z]+\.com(\.[a-z]{2})?/)) {
    throw new Error('validation error: invalid email format');
  }

  // Change: regex '/[a-z0-9]{6,20}/' changed for '/[a-zA-Z0-9]{6,20}/'
  // Change made for including capital letters in passwords
  if (!password.match(/[a-zA-Z0-9]{6,20}/)) {
    throw new Error('validation error: invalid password format');
  }

  if (password !== passwordConfirmation) {
    throw new Error('validation error: confirmation does not match');
  }
}

describe(createUser, () => {
  const validName = 'James';
  const validEmail = 'test@tmp.com.br';
  const validPassword = 'password';
  const validPasswordConfirmation = validPassword;

  it('valid arguments', () => {
    createUser(validName, validEmail, validPassword, validPasswordConfirmation);
  });

  it("email ends with '.com' [CHANGED BEHAVIOR]", () => {
    const testEmail = 'test@tmp.com';
    createUser(validName, testEmail, validPassword, validPasswordConfirmation);
  });

  it('password has capital letters [CHANGED BEHAVIOR]', () => {
    const testPassword = 'PASSWORD';
    const testPasswordConfirmation = testPassword;
    createUser(validName, validEmail, testPassword, testPasswordConfirmation);
  });

  it("requires email to follow format 'name@domain.com' \
  or 'name@domain.com.[TWO LOWERCASE LETTERS]'", () => {
    const invalidEmails = ['@test.com', 'test@test, test.com', 'simplestring'];

    invalidEmails.forEach(invalidEmail => {
      let safeEnv = () => {
        createUser(
          validName,
          invalidEmail,
          validPassword,
          validPasswordConfirmation
        );
      };

      expect(safeEnv).toThrow();
    });

    const localValidEmail = 'test@test.com';
    createUser(
      validName,
      localValidEmail,
      validPassword,
      validPasswordConfirmation
    );
  });

  it('password cannot contain characters other than alphanumeric ones', () => {
    const invalidPasswords = ['', 'pass-word', '#test', 'or&and'];

    invalidPasswords.forEach(invalidPassword => {
      const invalidPasswordConfirmation = invalidPassword;
      let safeEnv = () => {
        createUser(
          validName,
          validEmail,
          invalidPassword,
          invalidPasswordConfirmation
        );
      };

      expect(safeEnv).toThrow();
    });
  });
});
